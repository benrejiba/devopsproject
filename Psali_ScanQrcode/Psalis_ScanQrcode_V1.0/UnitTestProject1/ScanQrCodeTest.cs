﻿/* ***************************************************************************
   	*   NOM             :  ScanQrcodetest
   	-----------------------------------------------------------------------------
   	*   DATE CREATION   :01/10/2018
   	-----------------------------------------------------------------------------
   	*  AUTEUR                 : Bahri Saiffedine  benrejiba fares
   	-----------------------------------------------------------------------------
   	*   DESCRIPTION   :    GetPatientTest ,QrCodeTest
   	-----------------------------------------------------------------------------
   	*Historique des Modifications :01/10/2018 :   Méthode GetPatient   
                                 * 02/10/2018 :   QrCodeTest 
 
   	*************************************************************************** */
#region Using
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Psalis_ScanQrCode.DAL.Model;
    using Psalis_ScanQrCode.Properties;
    using Psalis_ScanQrCode.BLL.QrCode;

#endregion
#region ScanQrCodeTest
namespace Psalis_ScanQrCode.Test.QrCode
{/// <summary>
/// ScanQrCodeTest
/// </summary>
    [TestClass]
    public class ScanQrCodeTest
    { 
        //Initialisation
        PatientBusiness _Patient = new PatientBusiness();
        QrCodeBusiness _Qrcode = new QrCodeBusiness();
        /// <summary>
        /// GetPatientTest
        /// </summary>
        [TestMethod]
        public void  GetPatientTest()
        {
           ///Execution de la méthode GetPatient
            Patient patient =  _Patient.GetPatient("9803590224","6392");
            ///Test de résultat 
            Assert.AreEqual(patient.Name_Patient, "TEST7");
        }
        /// <summary>
        /// QrCodeTest cas1
        /// </summary>
        [TestMethod]
        public void QrCodeTest1()
        {  ///Initialisation  de cas de Test du QrCode1
            string QrCode1 = "0001;123456;98765";
            ///Execution de la méthode ScanQrCode 
            string patient1 = _Qrcode.ScanQrCode(QrCode1);
            ///Test de résultat
            Assert.AreEqual(patient1, Resources.ERROR_PATIENT);
        }
        /// <summary>
        /// QrCode cas2
        /// </summary>
        [TestMethod]
        public void QrCodeTest2()
        { ///Initialisation  de cas de Test du QrCode 
            string QrCode2 = "0001; ;98765";
            ///Execution de la méthode ScanQrCode 
            string patient2 = _Qrcode.ScanQrCode(QrCode2);
            ///Test de résultat
            Assert.AreEqual(patient2.ToString().Split('\n')[0], " First_Name:franck ");
        }
        /// <summary>
        /// QrCode cas 3
        /// </summary>
        [TestMethod]
        public void QrCodeTest3()
        { ///Initialisation  de cas de Test du QrCode 
            string QrCode3 = "0001;123456;";
            ///Execution de la méthode ScanQrCode 
            string patient3 = _Qrcode.ScanQrCode(QrCode3);
            ///Test de résultat
            Assert.AreEqual(patient3, Resources.ERROR_PATIENT);
        }
        /// <summary>
        /// QrCode cas 4
        /// </summary>
        [TestMethod]
        public void QrCodeTest4()
        { 
            ///Initialisation  de cas de Test du QrCode 
            string QrCode4 = "; 123456;98765";
            ///Execution de la méthode ScanQrCode 
            string patient4 = _Qrcode.ScanQrCode(QrCode4);
            ///Test de résultat
            Assert.AreEqual(patient4, Resources.ERROR_VERSION);
        }
        /// <summary>
        /// QrCode cas 5
        /// </summary>
        [TestMethod]
        public void QrCodeTest5()
        {
            ///Initialisation  de cas de Test du QrCode 
            string QrCode5 = " ; ;";
            ///Execution de la méthode ScanQrCode 
            string patient5 = _Qrcode.ScanQrCode(QrCode5);
            ///Test de résultat
            Assert.AreEqual(patient5, Resources.ERROR_VERSION);
        }
        /// <summary>
        /// QrCode cas 6
        /// </summary>
        [TestMethod]
        public void QrCodeTest6()
        {
            ///Initialisation  de cas de Test du QrCode 
            string QrCode6 = "0002;1;2";
            ///Execution de la méthode ScanQrCode 
            string patient6 = _Qrcode.ScanQrCode(QrCode6);
            ///Test de résultat
            Assert.AreEqual(patient6, Resources.ERROR_PATIENT);
        }
        /// <summary>
        /// QrCode cas 7
        /// </summary>
        [TestMethod]
        public void QrCodeTest7()
        {  
            ///Initialisation  de cas de Test du QrCode 
            string QrCode7 = "0001;9803590224;6392";
            //Execution de la méthode ScanQrCode 
            string patient7 = _Qrcode.ScanQrCode(QrCode7);
            ///Test de résultat
            Assert.AreEqual(patient7.ToString().Split('\n')[0], " First_Name:Test4 ");

        }
    }
}

#endregion