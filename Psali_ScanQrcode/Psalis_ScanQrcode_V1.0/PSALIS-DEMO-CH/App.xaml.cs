﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using PSALIS_DEMO_CH.Presentation.Ecrans;

namespace PSALIS_DEMO_CH
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        //private void Application_Startup(object sender, StartupEventArgs e)
        //{
        //    ScanQrCode mainWindow = new ScanQrCode();
        //    mainWindow.show();
        //}

            public static string GetResourceDictionary(string key)
            {
                if (string.IsNullOrEmpty(key)) return null;
                var resource = Current.Resources[key];
                if (resource == null)
                {

                    return key;
                    //throw new Exception("Resource not found with key: " + key);
                }
                return resource.ToString();
            }
        }
    }

