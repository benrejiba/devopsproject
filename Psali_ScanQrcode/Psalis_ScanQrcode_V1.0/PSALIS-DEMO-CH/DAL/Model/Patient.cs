﻿/* ***************************************************************************
   	*   NOM             :  Patient
   	-----------------------------------------------------------------------------
   	*   DATE CREATION   : 28/09/2018
   	-----------------------------------------------------------------------------
   	*  AUTEUR                 : Bahri Saiffedine  benrejiba fares
   	-----------------------------------------------------------------------------
   	*   DESCRIPTION   :  Model  Patient 
   	
   	-----------------------------------------------------------------------------
   	*Historique des Modifications :28/09/2018 : Model  Patient  
    *                               03/10/2018 :Méthode ToString()
 
   	*************************************************************************** */
#region Using

    using Newtonsoft.Json;
    using System;

#endregion
#region Classe_Patient
namespace Psalis_ScanQrCode.DAL.Model
{
    /// <summary>
    /// Model patient 
    /// </summary>
    public class Patient
    {
        ///déclaration des Attributs de  Model  Patient 
        [JsonProperty("center_code")]
        public string Center_Code_Patient { get; set; }
        [JsonProperty("ref")]
        public string Speciality_Patient  { get; set; }
        [JsonProperty("gender")]
        public string Gender_Patient      { get; set; }
        [JsonProperty("name")]
        public string Name_Patient        { get; set; }
        [JsonProperty("first_name")]
        public string First_Name_Patient  { get; set; }
        [JsonProperty("email")]
        public string Email_Patient        { get; set; }
        [JsonProperty("home_phone")]
        public string Home_Phone_Patient  { get; set; }
        [JsonProperty("mobile_phone")]
        public string Mobile_Phone_Patient{ get; set; }
        [JsonProperty("birth_date")]
        public string Birth_Date_Patient  { get; set; }
        [JsonProperty("address")]
        public string Address_Patient     { get; set; }
        [JsonProperty("zip_code")]
        public string Zip_Code_Patient    { get; set; }
        [JsonProperty("city")]
        public string City_Patient        { get; set; }

       // surcharge de la méthode ToString de Model Patient 
  public override string ToString()
        {
            return
                //surcharge de la méthode ToString
                String.Format(
                " First_Name:{0} \n Name:{1}\n Address:{2} \n Email {3}\n  Birth_Date:{4} \n Center_Code:{5} \n "
                +"City:{6} \n Gender:{7} \n Home_Phone:{8} \n Mobile_Phone:{9} \n Speciality:{10}\n Zip_Code:{11}"
                , this.First_Name_Patient, this.Name_Patient , this.Address_Patient ,this.Email_Patient, this.Birth_Date_Patient 
                , this.Center_Code_Patient , this.City_Patient , this.Gender_Patient , this.Home_Phone_Patient ,this.Mobile_Phone_Patient 
                , this.Speciality_Patient, this.Zip_Code_Patient);
        }
    }
}
#endregion