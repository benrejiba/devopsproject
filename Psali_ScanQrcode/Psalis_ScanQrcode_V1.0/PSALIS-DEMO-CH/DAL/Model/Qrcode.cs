﻿/* ***************************************************************************
   	*   NOM             : QrCode
   	-----------------------------------------------------------------------------
   	*   DATE CREATION   : 28/09/2018
   	-----------------------------------------------------------------------------
   	*  AUTEUR                 : Bahri Saiffedine  benrejiba fares
   	-----------------------------------------------------------------------------
   	*   DESCRIPTION   : Model Qrcode
   	
   	-----------------------------------------------------------------------------
   	*Historique des Modifications :28/09/2018 :   Model Qrcode 
 
   	*************************************************************************** */
#region Using

#endregion 
#region classe_QrCode

namespace Psalis_ScanQrCode.DAL.Model
{ 
    /// <summary>
    ///Model Qrcode  
    /// </summary>
    public class Qrcode
    { 
        //Déclaration des Attributs  de l'objet Qrcode  
        public string Version { get; set; }
        public string Ippshare { get; set; }
        public string Ippelisa { get; set; } 
        /// <summary>
        /// constructeur Parametré de class QrCode
        /// </summary>
        /// <param name="version"></param>
        /// <param name="ippshare"></param>
        /// <param name="ippelisa"></param>
        public Qrcode(string version,string  ippshare,string ippelisa)
        {
            Version = version;
            Ippshare = ippshare;
            Ippelisa = ippelisa;

        }
    } 
}
#endregion