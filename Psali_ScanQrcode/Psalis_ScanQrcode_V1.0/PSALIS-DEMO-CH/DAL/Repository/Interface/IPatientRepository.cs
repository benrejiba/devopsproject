﻿/* ***************************************************************************
   	*   NOM             :  IPatientRepository
   	-----------------------------------------------------------------------------
   	*   DATE CREATION   :01/10/2018
   	-----------------------------------------------------------------------------
   	*  AUTEUR                 : Bahri Saiffedine  benrejiba fares
   	-----------------------------------------------------------------------------
   	*   DESCRIPTION   :     Interface  des fonctionnalités   Patient 
   	
   	-----------------------------------------------------------------------------
   	*Historique des Modifications :01/10/2018 :   GetPatient 
                                        
   	*************************************************************************** */
#region Using 
using Psalis_ScanQrCode.DAL.Model;
#endregion
#region Interface PatientRepository 
namespace Psalis_ScanQrCode.DAL.Repository.Interface
{/// <summary>
/// Interface Patient
/// </summary>
    public  interface IPatientRepository
    {   /// <summary>
        /// GetPatient
        /// </summary>
        /// <param name="Ippshare"></param>
        /// <param name="Ippelisa"></param>
        /// <returns>Model Patient</returns>
        Patient GetPatient(string Ippshare, string Ippelisa); 

    }
}
#endregion
