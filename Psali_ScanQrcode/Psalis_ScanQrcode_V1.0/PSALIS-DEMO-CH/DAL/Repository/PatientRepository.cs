﻿/* ***************************************************************************
   	*   NOM             :  PatientRepository
   	-----------------------------------------------------------------------------
   	*   DATE CREATION   :01/10/2018
   	-----------------------------------------------------------------------------
   	*  AUTEUR                 : Bahri Saiffedine  benrejiba fares
   	-----------------------------------------------------------------------------
   	*   DESCRIPTION   :     Implémentation de la Méthode GetPatient 
   	
   	-----------------------------------------------------------------------------
   	*Historique des Modifications :01/10/2018 :   Méthode GetPatient  
   
   	*************************************************************************** */
#region Using
    using log4net;
    using Newtonsoft.Json;
    using Psalis_ScanQrCode.DAL.Model;
    using System;
    using System.Net;
    using System.Linq;
    using System.Collections.Generic;

#endregion
#region PatientRepository
namespace Psalis_ScanQrCode.DAL.Repository
   {/// <summary>
    /// Repository Patient 
    /// </summary>
    public class PatientRepository : Interface.IPatientRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(PatientRepository));
        /// <summary>
        /// GetPatient
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="Ippshare_Qrcode"></param>
        /// <param name="Ippelisa_Qrcode"></param>
        /// <returns>Model Patient</returns>
        public Patient GetPatient(string Ippshare, string Ippelisa)

        {
            Patient _patient = new Patient();
            try
            {
                string json_data;
                string _url = Properties.Settings.Default.PATH + "?extern-id=" + Ippshare + "&ipp-elisa=" + Ippelisa;
                // recupération de l'objet JSON 
                using (var web = new WebClient())
                {
                    json_data = web.DownloadString(_url);
                    _patient = JsonConvert.DeserializeObject<List<Patient>>(json_data).FirstOrDefault();
                }
                Log.Info("succés de Reponse de l'Api GetPatient");
                //retourne  l'objet Patient 
                return _patient;

            }
            //en cas d'exception 
            catch (Exception ex)
            {
                Log.Error("Erreur Api GetPatient" + ex.Message);
                return _patient;
            }
        }
    }
}
#endregion PatientRepository
