﻿/* ***************************************************************************
   	*   NOM             :  PatientBusiness
   	-----------------------------------------------------------------------------
   	*   DATE CREATION   :01/10/2018
   	-----------------------------------------------------------------------------
   	*  AUTEUR                 : Bahri Saiffedine  benrejiba fares
   	-----------------------------------------------------------------------------
   	*   DESCRIPTION   :     Implémentation de la couche  métier Patient 
   	
   	-----------------------------------------------------------------------------
   	*Historique des Modifications :01/10/2018 :   GetPatient
 
   	*************************************************************************** */
#region Using 
    using Psalis_ScanQrCode.DAL.Repository;
    using Psalis_ScanQrCode.DAL.Repository.Interface;
    using Psalis_ScanQrCode.DAL.Model;
#endregion
#region PatientBusiness
namespace Psalis_ScanQrCode.BLL.QrCode
{/// <summary>
/// Classe PatientBusiness
/// </summary>
    public  class PatientBusiness
    {    
        private  IPatientRepository _IPatientRepository = null;
       /// <summary>
       /// Constructeur de  classe PatientBusiness 
       /// </summary>
        public  PatientBusiness()
        {    
            this._IPatientRepository = new PatientRepository();
        }
        /// <summary>
        /// Lier interface IpatientRepository au classe PatientRepository
        /// </summary>
        /// <param name="_Patientrepository"></param>
        public   PatientBusiness(IPatientRepository _Patientrepository)
        {
            this._IPatientRepository = _Patientrepository;
        }
        /// <summary>
        /// GetPatient
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="Ippshare_Qrcode"></param>
        /// <param name="Ippelisa_Qrcode"></param>
        /// <returns>ModelPatient</returns>
        public  Patient GetPatient(string Ippshare , string Ippelisa)
        {
            return  _IPatientRepository.GetPatient(Ippshare, Ippelisa);
        }
    }
}
#endregion PatientBusiness 