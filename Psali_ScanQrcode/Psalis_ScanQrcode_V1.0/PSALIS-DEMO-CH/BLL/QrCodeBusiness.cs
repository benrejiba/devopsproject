﻿
/* ***************************************************************************
   	*   NOM             : QrCodeBusiness
   	-----------------------------------------------------------------------------
   	*   DATE CREATION   :  02/10/2018
   	-----------------------------------------------------------------------------
   	*  AUTEUR                 : Bahri Saiffedine  benrejiba fares
   	-----------------------------------------------------------------------------
   	*   DESCRIPTION   :     Implémentation de la Méthode ScanQrCode
   	
   	-----------------------------------------------------------------------------
   	*Historique des Modifications :02/10/2018 :   Implémentation de la Méthode ScanQrCodeAsync
 
   	*************************************************************************** */
#region Using 
    using log4net;
    using Psalis_ScanQrCode.DAL.Model;
    using Psalis_ScanQrCode.Properties;
    using System;
#endregion
#region QrCodeBusiness


namespace Psalis_ScanQrCode.BLL.QrCode
{/// <summary>
 /// classe QrCodeBusiness
 /// </summary>
    public class QrCodeBusiness
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(QrCodeBusiness));


        
        /// <summary>
        /// ScanQrCode
        /// </summary>
        /// <param name="QrCode"></param>
        /// <returns></returns>
        public string ScanQrCode (string QrCode)
        {  
            //initialisation 
            Patient _Patient = null;
            PatientBusiness _patientBusiness = new PatientBusiness();
            string[] _QrCode = QrCode.Split(';');
            try
            {
              
             
                // Test  de format QrCode 
                if (_QrCode.Length == 3 && _QrCode[0].Length == 4)
                {
                    //Test  de Version Qrcode
                    if (int.TryParse(_QrCode[0], out int _Version))
                    {
                        //instanciation de  QrCode avec les données du Code scanné 
                        Qrcode ModelQrCode = new Qrcode(_QrCode[0], _QrCode[1], _QrCode[2]);
                       //Appelle de  methode GetPatient 
                      _Patient =  _patientBusiness.GetPatient(ModelQrCode.Ippshare, ModelQrCode.Ippelisa);                      
                      Log.Info(Resources.SUCCES_PATIENT); 
                        //Retourne le détail du patient  
                        return _Patient.ToString();
                    }
                    else
                    { 
                        //message d'erreur de Version 
                        Log.Error(Resources.ERROR_VERSION);                        
                        return Resources.ERROR_VERSION;
                    }
                }
                else
                {
                    //message d'erreur de Format 
                    Log.ErrorFormat(Resources.ERROR_VERSION);
                    return Resources.ERROR_VERSION;
                } 
                }             
               catch (Exception ex)
            {
                // message en cas le Patient n'existe pas 
                Log.Error(Resources.ERROR_PATIENT + ex.Message);
                return Resources.ERROR_PATIENT;
            }
  

        }
    }

}

#endregion

