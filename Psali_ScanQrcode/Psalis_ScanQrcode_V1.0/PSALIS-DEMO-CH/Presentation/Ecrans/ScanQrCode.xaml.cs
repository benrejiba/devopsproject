﻿/* **********************************************************************************************
   	*   NOM             : QrCodeBusiness
   	-----------------------------------------------------------------------------
   	*   DATE CREATION   :  02/10/2018
   	-----------------------------------------------------------------------------
   	*  AUTEUR                 : Bahri Saiffedine  benrejiba fares
   	-----------------------------------------------------------------------------
   	*   DESCRIPTION   :     Implémentation de la Méthode ScanQrCode
   	
   	-----------------------------------------------------------------------------
   	*Historique des Modifications :04/10/2018 :   Implémentation de la Méthode  TxtQRCode_TextChanged
 
   	************************************************************************************************ */
#region Using
using System;
using System.Windows;
using System.Windows.Controls;
using log4net;
using Psalis_ScanQrCode.BLL.QrCode;
#endregion
#region ScanQrCode

namespace PSALIS_DEMO_CH.Presentation.Ecrans
{
    /// <summary>
    /// Logique d'interaction pour Psalis.xaml
    /// </summary>
    public partial class ScanQrCode : UserControl
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ScanQrCode));

        /// <summary>
        /// Inisialisation des Composants
        /// </summary>     
        public ScanQrCode()
        {
            InitializeComponent();

        } 
        /// <summary>
        /// Load UserControl
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            log.Info("Load Interface ");
            TxtQRCode.Focus();

        }
      /// <summary>
      /// click bouton
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
        private void BtnSansConvocation_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Bouton " + ((Button)sender).Name);
            MessageBox.Show("veuillez contacter tecsanté");
        }

        /// <summary>
        /// Détection d'un QR Code scanné 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtQRCode_TextChanged(object sender, TextChangedEventArgs e)
        {
            log.Info("textbox"+((TextBox)sender).Name);
            ///test la fin de lecture de Qr code 
            if (TxtQRCode.Text.Contains("\n"))
            {   ///Appel de la methode scanQrcode 
                QrCodeBusiness _QrCodeBusiness = new QrCodeBusiness();
                string _patient = _QrCodeBusiness.ScanQrCode(TxtQRCode.Text.Trim('\r', '\n'));
                MessageBox.Show(_patient);
                TxtQRCode.Text = String.Empty;
                log.Info(_patient);

            }
        }

       
    }
        
      
    }
#endregion
